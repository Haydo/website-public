<?php
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

$admin_email = 'hayden@haydenives.com'; // Your Email
$message_min_length = 5; // Min Message Length


class Contact_Form{
	function __construct($details, $email_admin, $message_min_length){
		$this->name = stripslashes($details['name']);
		$this->email = trim($details['email']);
		$this->subject = stripslashes($details['subject']);
		$this->message = stripslashes($details['message']);
	
		$this->email_admin = $email_admin;
		$this->message_min_length = $message_min_length;
		
        $this->response_status = 1;
		$this->response = '';
	}


	private function validateEmail(){
		$regex = '/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i';
	
		if($this->email == '') {
			return false;
		} else {
			$string = preg_replace($regex, '', $this->email);
		}
	
		return empty($string) ? true : false;
	}


	private function validateFields(){
		// Check name
		if(!$this->name)
		{
			$this->response .= 'Name is a required field.';
			$this->response_status = 0;
		}

		// Check email
		elseif(!$this->email)
		{
			$this->response .= 'Email is a required field.';
			$this->response_status = 0;
		}
		
		// Check valid email
		elseif($this->email && !$this->validateEmail())
		{
			$this->response .= 'It looks like that email is invalid.';
			$this->response_status = 0;
		}

		// Check for subject 
		elseif(!$this->subject) 
		{
			$this->response .= 'Subject is a required field.';
			$this->response_status = 0;
		}
		
		// Check message length
		elseif(!$this->message)
		{
			$this->response .= 'Message is a required field.';
			$this->response_status = 0;
		} 
		
		elseif($this->message && strlen($this->message) < $this->message_min_length)
		{
			$this->response .= 'Ensure your message is at least '.$this->message_min_length.' characters long.';
			$this->response_status = 0;
		}
	}


	private function sendEmail(){
		$mail = mail($this->email_admin, $this->subject, $this->message,
			 "From: ".$this->name." <".$this->email.">\r\n"
			."Reply-To: ".$this->email."\r\n"
		."X-Mailer: PHP/" . phpversion());
	
		if($mail)
		{
			$this->response_status = 1;
			$this->response = 'Message sent successfully!';
		}
	}


	function sendRequest(){
		$this->validateFields();
		if($this->response_status)
		{
			$this->sendEmail();
		}

		$response = array();
		$response['status'] = $this->response_status;
		$response['body'] = $this->response;
		
		echo json_encode($response);
	}
}


$contact_form = new Contact_Form($_POST, $admin_email, $message_min_length);
$contact_form->sendRequest();

?>