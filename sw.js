var CACHE_NAME = 'hayden-ives-site-v1';
var urlsToCache = [
    '/',
    '/css/anim.css',
    '/css/index.css',
    '/css/mobile.css',
    '/css/scrollbar.css',
    '/js/data.js',
    '/js/shorthand.js',
    '/js/index.js'
];

self.addEventListener('install', (event) => {
    // Perform install steps
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then((cache) => {
                console.log('Opened cache');
                for(let i = 0; i < urlsToCache.length; i++) {
                    cache.add(urlsToCache[i])
                }
                return cache;
            })
    );
});

self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request)
            .then((response) => {
                // Cache hit - return response
                if (response) {
                    return response;
                }
                return fetch(event.request);
            }
            )
    );
});